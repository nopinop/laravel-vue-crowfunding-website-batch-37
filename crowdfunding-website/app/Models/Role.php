<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traints\UsesUuid;


class Role extends Model
{
    use UsesUuid;

    protected $fillable = ['name'];

    public function users(){
        return $this->hasMany('App\User', 'role_id');
    }
}
