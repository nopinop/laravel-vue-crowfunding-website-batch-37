<?php 

trait hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function attraksi() {
        echo "{$this->nama} sedang {$this->keahlian}";
    }

}

abstract class fight {
    use hewan;
    public $attackPower;
    public $defensePower;

    public function serang($hewan) {
        echo "{$this->nama} sedang menyerang {$hewan->nama}";
        $hewan->diserang($this->diserang);
    }

    public function diserang($hewan) {
        echo "{$this->nama} sedang diserang";
        $this->darah = $this->darah - ($hewan->attackPower / $hewan->defensePower);
    }

    protected function getInfo() {
        echo "<br>";
        echo "nama : ($this->nama)";
        echo "<br>";
        echo "jumlahKaki : ($this->jumlahKaki)";
        echo "<br>";
        echo "keahlian : ($this->keahlian)";
        echo "<br>";
        echo "darah : ($this->darah)";
        echo "<br>";
        echo "Attack Power : ($this->attackPower)";
        echo "<br>";
        echo "Defense Power : ($this->defensePower)";
        echo "<br>";
        $this->attraksi();
    }

    abstract public function getInfoHewan();

}

class Elang extends fight {
    public function __construct($string) {
        $this->nama = $string;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defensePower = 5;
    }

    public function getInfoHewan(){
        echo "Jenis Hewan : Elang";
        $this->getInfo();
    }
}

class Harimau extends fight {
    public function __construct($string) {
        $this->nama = $string;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defensePower = 8;
    }

    public function getInfoHewan(){
        echo "Jenis Hewan : Harimau";
        $this->getInfo();
    }
}

class jarak{
    public static function kasihJarak(){
        echo "<br> <br>";
    }
}

$elang = new Elang("Elang");
$elang->getInfoHewan();

jarak::kasihJarak();

$harimau = new Harimau("Harimau"); 
$harimau->getInfoHewan();

jarak::kasihJarak();

$elang->diserang($harimau);
jarak::kasihJarak();
$elang->getInfoHewan();
jarak::kasihJarak();
$harimau->diserang($elang);
jarak::kasihJarak();
$harimau->getInfoHewan();




?>